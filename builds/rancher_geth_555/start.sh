#!/bin/bash -x

echo "$GETH_WALLET_CLIQUE_PASSWORD" > /root/pass.txt

if ! [ -d /root/.ethereum/555/ ]; then
mkdir -p /root/.ethereum/555/keystore
cp /root/genesis.json /root/.ethereum/555/genesis.json    
cp /root/wallets/* /root/.ethereum/555/keystore
geth init /root/.ethereum/555/genesis.json --datadir /root/.ethereum/555
else
    if [[ $TYPE = 'PRIVATE_NO_SIGN' ]]; then
        geth \
            --port 30303 \
            --networkid 555 \
            --ws \
            --wsaddr 0.0.0.0 \
            --wsorigins "*" \
            --wsport 8544 \
            --rpc \
            --rpcaddr 0.0.0.0 \
            --rpcport 8543 \
            --rpcapi "db,eth,net,web3,personal,miner,admin,clique" \
            --rpcvhosts "*" \
            --rpccorsdomain "*" \
            --targetgaslimit 9000000 \
            --datadir /root/.ethereum/555 \
            --bootnodes "enode://5bb9db59ade78531def774570efe2c5fd25cb3a435b84f75568769f7688bd09c38c3d26e7fca69979ff47658aed404366c79736d2fb5ec9ba10e330d84724328@35.205.72.14:30303"
    fi
    if [[ $TYPE = 'PRIVATE_SIGN' ]]; then
        geth \
            --port 30303 \
            --networkid 555 \
            --ws \
            --wsaddr 0.0.0.0 \
            --wsorigins "*" \
            --wsport 8544 \
            --rpc \
            --rpcaddr 0.0.0.0 \
            --rpcport 8543 \
            --rpcapi "db,eth,net,web3,personal,miner,admin,clique" \
            --rpcvhosts "*" \
            --rpccorsdomain "*" \
            --targetgaslimit 9000000 \
            --datadir /root/.ethereum/555 \
            --mine \
            --unlock "0x4eafd792f707fb00277659381cf8e419c7bd9602" \
            --password "/root/pass.txt" \
            --minerthreads 1 \
            --etherbase "0x4eafd792f707fb00277659381cf8e419c7bd9602" \
            --bootnodes "enode://5bb9db59ade78531def774570efe2c5fd25cb3a435b84f75568769f7688bd09c38c3d26e7fca69979ff47658aed404366c79736d2fb5ec9ba10e330d84724328@35.205.72.14:30303"
    fi   

fi