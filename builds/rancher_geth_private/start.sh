#!/bin/bash -x

if ! [ -d /root/.ethereum/geth/ ]; then
mkdir -p /root/.ethereum/keystore
cp /root/genesis.json /root/.ethereum/genesis.json
cp /root/UTC--2018-02-01T03-59-50.577684101Z--f03dceff5642cd5eeb0c093c7e896686f348d878 /root/.ethereum/keystore/UTC--2018-02-01T03-59-50.577684101Z--f03dceff5642cd5eeb0c093c7e896686f348d878
geth init /root/.ethereum/genesis.json
fi